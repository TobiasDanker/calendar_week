$(document).ready(function() {
	var style = $('<style>.calendar_week { max-width: ' + Math.min(455, $(document).width() - 154)  + 'px; }</style>');
	$('html > head').append(style);
	var spinner = $('#new_calendar_week_timeShift').spinner({
		min: -12, 
		max: 12,
		spin: function(data) {
			setTimeout(function () {
				if(parseInt(spinner.val()) > -1) {
					spinner.val('+' + spinner.val());
				}	
			});		
		}
	});
	spinner.val('+0');
	$('#new_calendar_week').css('width', Math.min(492, $(document).width() - 129)  + 'px');
});


var validatingCalendar = false;

function validateWeekCalendar() {
	if(!validatingCalendar) {
		$('#validate_calendar_week').show();
		$('#invalid_calendar_week').hide();
		validatingCalendar = true;
		var calendar = $('#new_calendar_week').val().trim();
    	if(calendar.length > 0) {  
		    $.post("../modules/calendar_week/assets/isCalendar.php", {url: calendar})
			.done(function(data) {
				$('#validate_calendar_week').hide();
				
				if(data == '1') {
					$('#calendar_week__validate').hide();
					$('#calendar_week__add').show();
				} else {
					$('#calendar_week__add').hide('fast');
					$('#invalid_calendar_week').show();
				} 
			})
			.complete(function() {
				validatingCalendar = false;
				if(calendar != $('#new_calendar_week').val().trim()) {
					validateWeekCalendar();
				}
				
			});
    	} else {
    		$('#validate_calendar_week').hide();
    	}
	}
};

$('#calendar_week__validate').click(validateWeekCalendar);

$('#new_calendar_week').on('input', function() {
		$('#calendar_week__add').hide();
		$('#calendar_week__validate').hide();
		var calendar = $('#new_calendar_week').val().trim();
    	if(calendar.length > 0) {  
    		$('#calendar_week__validate').show();
    	}  		
});

function writeTodayCalendars() {
	$.post('setConfigValueAjax.php', {'key' : 'calendar_week_calendars', 'value' : JSON.stringify(prepareCalendarTodayData())})
		.done(function() { 
			$('#ok').show(30, function() {
				$(this).hide('slow');
			})
		});
};

function prepareCalendarTodayData() {
	data = [];
	$(".calendar_week").each(function(i, element){ 
		data[i] = {};
		data[i]['label'] = $(element).parent().siblings(':first-child').text();
		data[i]['name'] = $(element).text();
		data[i]['timeShift'] = $('.calendar_week_timeShift').eq(i).text();
	});

	return data;
}

$('#calendar_week__add').click(function() {
	console.log($('.calendar_week_category').length);
	if($('#new_calendar_week_label').val().trim().length < 1 || ($('.calendar_week_category').length == 5 && !$('#calendar_week_' + $('#new_calendar_week_label').val()).length)) {
		$('#new_calendar_week_label').addClass('error');
		return;
	}
	
	var newEntry = '<p style="border: 1px solid #ddd; height: 35px;">' + 
	'	<button class="calendar_week__edit">' +
	'	<span class="fi-pencil"></span></button>' +
	'	<span class="calendar_week">' + $('#new_calendar_week').val().trim() + '</span>' +
	' 	<span class="parent_calendar_week_timeShift"> (<span class="calendar_week_timeShift">' + $('#new_calendar_week_timeShift').val() + '</span>h)</span>' +
	'	<button style="position: absolute; right: 30px;" class="calendar_week__delete" href="#">' +
	'	<span class="fi-trash"></span></button></p>';
	if($('#calendar_week_' + $('#new_calendar_week_label').val()).length ) {
		$('#calendar_week_' + $('#new_calendar_week_label').val()).after(newEntry);
	} else {
		$('#new_calendar_week_label').before('<div class="calendar_week_category"><div class="bold" id="calendar_week_' + $('#new_calendar_week_label').val() + '">' + $('#new_calendar_week_label').val() + '</div>' + newEntry + '</div>');
	}

	$('#new_calendar_week').val('');
	$('#new_calendar_week_label').val('');
	$('#new_calendar_week_timeShift').spinner('value', 0);
	$('#calendar_week__add').hide('fast');
	writeTodayCalendars();	
});

$(document).on('click', '.calendar_week__delete', function() {
    $(this).parent().hide('fast', function() {
    	if($(this).siblings().length == 1) {
    		$(this).parent().remove();
    	} else {
    		$(this).remove();
    	}
	    writeTodayCalendars();
	});  
});

$(document).on('click', '.calendar_week__edit', function() {
	$('#new_calendar_week').val($(this).siblings('.calendar_week').text());
	$('#new_calendar_week').trigger('input');
	$('#new_calendar_week_label').val($(this).parent().siblings(':first-child').text());
	var timeShift = $(this).siblings('.parent_calendar_week_timeShift').children('.calendar_week_timeShift').text().toString();
	$('#new_calendar_week_timeShift').spinner('value', timeShift);
    $(this).parent().hide('fast', function() {
    	if($(this).siblings().length == 1) {
    		$(this).parent().remove();
    	}
	    $(this).remove();
	});  
});